# MobileStream
This repository is based on the work done for the MobileStream framework, published in CoNEXT 2018.

We will make source codes public and provide a testbed to run MobileStream.    
If you have any questions, please contact junguk.cho@utah.edu
